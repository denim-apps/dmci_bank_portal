from frappe import _

def get_data():
	return [
		{
			"module_name": "DMCI Bank Portal",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("DMCI Bank Portal")
		}
	]
