import json
import frappe
from frappe import permissions
from frappe import lark
from frappe.email.doctype.email_template.email_template import get_email_template
from frappe.utils.oauth import get_email, update_oauth_user
import requests

__version__ = '0.0.1'

@frappe.whitelist()
def endorse(contracts, banks):
  if not ('Account Officer' in frappe.get_roles(frappe.session.user)):
    return

  contracts = json.loads(contracts)
  banks = json.loads(banks)

  for contract in contracts:
    record = frappe.get_doc('Customer Account', contract)
    
    for bank in banks:
      has_bank = False

      for existing_bank in record.get('endorsed_banks'):
        if existing_bank.bank == bank:
          has_bank = True

      if not has_bank:
        record.append('endorsed_banks', {
          'bank': bank
        })

    record.has_endorsements = True
    record.save()

@frappe.whitelist()
def get_endorsed_banks(contracts):
  contracts = json.loads(contracts)
  banks = []

  for contract in contracts:
    record = frappe.get_doc('Customer Account', contract)

    for bank in record.endorsed_banks:
      if not bank.bank in banks:
        banks.append(bank.bank)

  return banks

def is_active_dmci_portal_user(email):
  return not frappe.db.get_value('DMCI Portal User', filters={'name': email}, fieldname='active') or False

@frappe.whitelist()
def award(contracts, bank):
  if not ('Account Officer' in frappe.get_roles(frappe.session.user)):
    return

  contracts = json.loads(contracts)

  for contract in contracts:
    record = frappe.get_doc('Customer Account', contract)
    
    if record.has_endorsements:
      record.log_bank = bank
      record.endorsed_banks = []
      record.save()

      # Send the notification
      dict = record.get_valid_dict()
      dict['bank'] = bank
      email = get_email_template('Bank Awarded', dict)

      # Find TLs of the bank
      emails = frappe.db.get_all('DMCI Portal User', filters={
        'bank': bank,
        'team_lead': True,
        'active': True,
      }, pluck='user')

      # Find assigned AO(s)
      aos = frappe.db.get_all('Account Bank Data', filters={
        'bank': bank,
        'customer_account': contract,
      }, pluck='ao')

      for ao in aos:
        if is_active_dmci_portal_user(ao):
          emails.append(ao)

      frappe.sendmail(
        recipients = emails,
        subject = email['subject'],
        message = email['message'],
      )

def get_dmci_portal_user(name):
  if frappe.db.exists('DMCI Portal User', name):
    doc = frappe.get_doc('DMCI Portal User', name)
  else:
    doc = frappe.new_doc('DMCI Portal User')
    doc.user = name

  return doc

def sync_user(user):
  # Fill in sub field for OAuth compliance
  user['sub'] = user['open_id']
  user['gender'] = ''
  new_user = frappe.get_value('User Social Login', filters={'provider': 'lark', 'userid': user['open_id']}, fieldname='parent') or get_email(user)
  update_oauth_user(new_user, user, 'lark')

  return new_user

@frappe.whitelist(allow_guest=True)
def sync_current_user():
  lark_settings = frappe.get_doc('Lark Settings')
  dmci_settings = frappe.get_doc('DMCI Settings')
  tenant_access_token = lark_settings.get_app_access_token()

  current_user = frappe.get_doc('User', frappe.session.user)
  user_open_id = None

  for login in current_user.social_logins:
    if login.get('provider') == 'lark':
      user_open_id = login.get('userid')
  
  if user_open_id:
    portal_user = get_dmci_portal_user(frappe.session.user)

    # Find their department
    user_data = requests.get('https://open.larksuite.com/open-apis/contact/v3/users/' + user_open_id, headers={
      'Authorization': 'Bearer ' + tenant_access_token
    }).json().get('data').get('user')

    if len(user_data.get('department_ids')):
      user_department_id = user_data.get('department_ids')[0]
      user_department = requests.get('https://open.larksuite.com/open-apis/contact/v3/departments/' + user_department_id, headers={
        'Authorization': 'Bearer ' + tenant_access_token
      }).json().get('data').get('department')

      parent_department = requests.get('https://open.larksuite.com/open-apis/contact/v3/departments/' + (user_department.get('parent_department_id') or '0'), headers={
        'Authorization': 'Bearer ' + tenant_access_token
      }).json().get('data').get('department')

      if parent_department.get('department_id') == dmci_settings.dmci_department_id or parent_department.get('department_id') == dmci_settings.bank_department_id:
        if not frappe.db.exists('DMCI Team' if parent_department.get('department_id') == dmci_settings.dmci_department_id else 'Bank', { 'lark_id': user_department.get('department_id') }):
          if parent_department.get('department_id') == dmci_settings.dmci_department_id:
            team_record = frappe.new_doc('DMCI Team')
            team_record.set('team_name', user_department.get('name'))
            team_record.set('lark_id', user_department.get('department_id'))
            team_record.save(ignore_permissions=True)

          if parent_department.get('department_id') == dmci_settings.bank_department_id:
            bank_record = frappe.new_doc('Bank')
            bank_record.set('bank_name', user_department.get('name'))
            bank_record.set('lark_id', user_department.get('department_id'))
            bank_record.save(ignore_permissions=True)

        if user_data['employee_type'] == 7 or user_data['employee_type'] == 9:
          portal_user.bank = None
          portal_user.dmci_team = user_department['name']
          portal_user.team_lead = user_data['employee_type'] == 9
          portal_user.active = True
          portal_user.flags['ignore_modified'] = True
          portal_user.save(ignore_permissions=True)

          for role in current_user.get('roles'):
            if role.role in ['Bank Officer']:
              current_user.remove(role)

          current_user.append('roles', {
            'doctype': 'Has Role',
            'role': 'Account Officer',
					})
          current_user.flags['ignore_modified'] = True
          current_user.save(ignore_permissions=True)

        if user_data['employee_type'] == 6 or user_data['employee_type'] == 8:
          portal_user.bank = user_department['name']
          portal_user.dmci_team = None
          portal_user.team_lead = user_data['employee_type'] == 8
          portal_user.active = True
          portal_user.flags['ignore_modified'] = True
          portal_user.save(ignore_permissions=True)

          for role in current_user.get('roles'):
            if role.role in ['Account Officer']:
              current_user.remove(role)

          current_user.append('roles', {
            'doctype': 'Has Role',
            'role': 'Bank Officer'
					})
          current_user.flags['ignore_modified'] = True
          current_user.save(ignore_permissions=True)

        frappe.db.commit()


@frappe.whitelist(allow_guest=True)
def sync_departments():
  lark_settings = frappe.get_doc('Lark Settings')
  dmci_settings = frappe.get_doc('DMCI Settings')
  app_access_token = lark_settings.get_app_access_token()

  # Sync departments
  dmci_departments = requests.get('https://open.larksuite.com/open-apis/contact/v3/departments?department_id_type=department_id&parent_department_id=' + dmci_settings.dmci_department_id, headers={
    'Authorization': 'Bearer ' + app_access_token
  }).json().get('data').get('items')

  bank_departments = requests.get('https://open.larksuite.com/open-apis/contact/v3/departments?department_id_type=department_id&parent_department_id=' + dmci_settings.bank_department_id, headers={
    'Authorization': 'Bearer ' + app_access_token
  }).json().get('data').get('items')

#  dmci_department_ids = []
#  bank_department_ids = []

  frappe.db.delete('DMCI Team')

  for department in dmci_departments:
    name = department['name']
    lark_id = department['department_id']
    team_record = frappe.new_doc('DMCI Team')
    team_record.set('team_name', name)
    team_record.set('lark_id', lark_id)
    team_record.save(ignore_permissions=True)
#    dmci_department_ids.append(lark_id)

  frappe.db.delete('Bank')

  for department in bank_departments:
    name = department['name']
    lark_id = department['department_id']
    bank_record = frappe.new_doc('Bank')
    bank_record.set('bank_name', name)
    bank_record.set('lark_id', lark_id)
    bank_record.save(ignore_permissions=True)
#    bank_department_ids.append(lark_id)

#  dmci_filters = []
#  bank_filters = []
#
#  for id in dmci_department_ids:
#    dmci_filters.append([
#      'lark_id',
#      '!=',
#      id,
#    ])
#
#  for id in bank_department_ids:
#    bank_filters.append([
#      'lark_id',
#      '!=',
#      id,
#    ])

  frappe.db.sql("""
    UPDATE `tabDMCI Portal User`
    SET `active` = 0
  """)

  # Retrieve DMCI department officers
  for department in dmci_departments:
    users = requests.get('https://open.larksuite.com/open-apis/contact/v3/users?department_id_type=department_id&department_id=' + department['department_id'], headers={
      'Authorization': 'Bearer ' + app_access_token
    }).json().get('data').get('items') or []

    for user in users:
      if user['employee_type'] == 7 or user['employee_type'] == 9:
        # Found account officer for this team
        # Find the user
        user_name = frappe.db.get_value('User Social Login', filters={ 'provider': 'lark', 'userid': user['open_id'] }, fieldname='parent')
        if not user_name:
          # Check for existing user with this email
          existing_social_login = frappe.db.get_value('User Social Login', filters={ 'provider': 'lark', 'parent': user['email'] }, fieldname='name')

          if existing_social_login:
            user_name = user['email']
            frappe.db.set_value('User Social Login', existing_social_login, 'userid', user['open_id'])
          else:
            user_name = sync_user(user)

        if user_name:
          portal_user = get_dmci_portal_user(user_name)
          portal_user.bank = None
          portal_user.dmci_team = department['name']
          portal_user.team_lead = user['employee_type'] == 9
          portal_user.active = True
          portal_user.flags['ignore_modified'] = True
          portal_user.save(ignore_permissions=True)

          user_record = frappe.get_doc('User', user_name)

          for role in user_record.get('roles'):
            if role.role in ['Bank Officer']:
              user_record.remove(role)

          user_record.append('roles', {
            'doctype': 'Has Role',
            'role': 'Account Officer',
					})
          user_record.flags['ignore_modified'] = True
          user_record.save(ignore_permissions=True)

  # Retrieve bank department officers
  for department in bank_departments:
    users = requests.get('https://open.larksuite.com/open-apis/contact/v3/users?department_id_type=department_id&department_id=' + department['department_id'], headers={
      'Authorization': 'Bearer ' + app_access_token
    }).json().get('data').get('items') or []

    for user in users:
      if user['employee_type'] == 6 or user['employee_type'] == 8:
        # Found account officer for this bank
        # Find the user
        user_name = frappe.db.get_value('User Social Login', filters={ 'provider': 'lark', 'userid': user['open_id'] }, fieldname='parent')

        if not user_name:
          # Check for existing user with this email
          existing_social_login = frappe.db.get_value('User Social Login', filters={ 'provider': 'lark', 'parent': user['email'] }, fieldname='name')

          if existing_social_login:
            user_name = user['email']
            frappe.db.set_value('User Social Login', existing_social_login, 'userid', user['open_id'])
          else:
            user_name = sync_user(user)

        if user_name:
          portal_user = get_dmci_portal_user(user_name)
          portal_user.bank = department['name']
          portal_user.dmci_team = None
          portal_user.team_lead = user['employee_type'] == 8
          portal_user.active = True
          portal_user.flags['ignore_modified'] = True
          portal_user.save(ignore_permissions=True)

          user_record = frappe.get_doc('User', user_name)

          for role in user_record.get('roles'):
            if role.role in ['Account Officer']:
              user_record.remove(role)

          user_record.append('roles', {
            'doctype': 'Has Role',
            'role': 'Bank Officer'
					})
          user_record.flags['ignore_modified'] = True
          user_record.save(ignore_permissions=True)

  frappe.db.commit()

#  dmci_to_delete = frappe.db.get_all('DMCI Team', filters=dmci_filters)
#  banks_to_delete = frappe.db.get_all('Bank', filters=bank_filters)
#
#  for team in dmci_to_delete:
#    frappe.delete_doc('DMCI Team', team.name, ignore_permissions=True)
#
#  for bank in banks_to_delete:
#    frappe.delete_doc('Bank', bank.name, ignore_permissions=True)
#
#  frappe.db.commit()

@frappe.whitelist(allow_guest=True)
def login_callback():
  lark.login_callback()
  sync_current_user()
  frappe.enqueue('dmci_bank_portal.sync_departments')
  frappe.local.response['type'] = 'redirect'
  frappe.local.response['location'] = '/app'

original_has_child_permission = permissions.has_child_permission

def has_child_permission(*args, **kwargs):
  if args[0] == 'Contract' or args[0] == 'Customer Account Bank' or args[0] == 'Customer Account User':
    return True

  return original_has_child_permission(*args, **kwargs)

permissions.has_child_permission = has_child_permission
