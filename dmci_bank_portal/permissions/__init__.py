from dmci_bank_portal import get_dmci_portal_user
import frappe
from frappe.model.rename_doc import rename_doc

def customer_account_filter(user):
  if not user:
    user = frappe.session.user

  roles = frappe.get_roles(user)
  portal_user = get_dmci_portal_user(user)

  query = '0'

  if 'System Manager' in roles:
    return '1'

  if 'Account Officer' in roles:
    if portal_user.team_lead:
      query += ' OR 1'
    else:
      query += ' OR dmci_ao = {ao}' \
        .format(ao=frappe.db.escape(user))

  if 'Bank Officer' in roles:
    # Find bank officer's bank
    bank = portal_user.bank

    if bank:
      query += ' OR (`tabCustomer Account`.account_final_status NOT IN ("BF DRAWN", "CHANGED TERM", "CANCELLED") AND (((SELECT COUNT(*) FROM `tabCustomer Account Bank` WHERE bank = {bank} AND parent = `tabCustomer Account`.name AND parentfield={endorsed_banks}) > 0 AND log_bank IS NULL) OR log_bank = {bank})' \
        .format(
          bank=frappe.db.escape(bank),
          endorsed_banks=frappe.db.escape('endorsed_banks'),
          bf_drawn=frappe.db.escape('BF DRAWN')
        )

      if not portal_user.team_lead:
        query += ' AND (SELECT COUNT(*) FROM `tabAccount Bank Data` assignment WHERE assignment.ao = {ao} AND assignment.customer_account = `tabCustomer Account`.name) > 0' \
          .format(
            ao=frappe.db.escape(user),
          )

      query += ')'

  return query

def user_filter(user):
  if not user:
    user = frappe.session.user

  roles = frappe.get_roles(user)
  portal_user = get_dmci_portal_user(user)

  query = '('

  if 'System Manager' in roles or 'Accounts Uploader' in roles:
    return '1'

  if portal_user.bank:
    return '((SELECT pu.bank FROM `tabDMCI Portal User` pu WHERE pu.user = `tabUser`.name) = {bank})'.format(bank=frappe.db.escape(portal_user.bank))

  if portal_user.dmci_team:
    return '((SELECT pu.dmci_team FROM `tabDMCI Portal User` pu WHERE pu.user = `tabUser`.name) = {dmci_team})'.format(dmci_team=frappe.db.escape(portal_user.dmci_team))

  return query

def customer_account_has_permission(doc, user=None, permission_type=None):
  roles = frappe.get_roles(user)

  if 'System Manager' in roles or 'Account Officer' in roles:
    return True

  if 'Bank Officer' in roles:
    if doc.account_final_status == 'BF DRAWN' or doc.account_final_status == 'CHANGED TERM' or doc.account_final_status == 'CANCELLED':
      return False

    # Find bank officer's bank
    portal_user = get_dmci_portal_user(user)
    bank = portal_user.bank

    if doc.log_bank == bank:
      return True

    if not doc.log_bank:
      for endorsed_bank in doc.endorsed_banks:
        if endorsed_bank.bank == bank:
          if portal_user.team_lead:
            return True
          else:
            return doc.bank_ao == user

  return False

def user_after_rename(doc, event, old_name, new_name, merge=False):
  if frappe.db.exists("DMCI Portal User", old_name):
    rename_doc("DMCI Portal User", old_name, new_name, force=True, show_alert=False, ignore_permissions=True)
