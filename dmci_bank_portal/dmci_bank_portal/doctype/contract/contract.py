# Copyright (c) 2021, JC Gurango and contributors
# For license information, please see license.txt

# import frappe
import frappe
from frappe.model.document import Document
from dmci_bank_portal.dmci_bank_portal.doctype.customer_account.customer_account import account_fields
import dateutil.parser as parser
import logging

class Contract(Document):
	def db_insert(self):
		new_account = frappe.new_doc('Customer Account')
		existing_account = frappe.get_doc('Customer Account', self.parent, is_child = True)

		if self.get('reference_row'):
			reference_account = frappe.get_doc('Customer Account', self.reference_row, is_child = True)

			for reference_field in reference_account.meta.fields:
				if reference_field.fieldname != 'name' and reference_field.fieldtype != 'Column Break' and reference_field.fieldtype != 'Section Break' and reference_field.fieldtype != 'Button':
					setattr(new_account, reference_field.fieldname, getattr(reference_account, reference_field.fieldname))

		for account_field in account_fields:
			setattr(new_account, account_field, getattr(existing_account, account_field))

		for contract_field in self.meta.fields:
			if contract_field.fieldname != 'name' and contract_field.fieldtype != 'Column Break' and contract_field.fieldtype != 'Section Break' and contract_field.fieldtype != 'Button':
				setattr(new_account, contract_field.fieldname, getattr(self, contract_field.fieldname))

		new_account.contracts = None
		new_account.name = None
		new_account.save()
		self.name = new_account.name

	def db_update(self):
		if self.get("__islocal") or not self.name:
			self.db_insert()
			return

		if self.name != self.parent:
			existing_account = frappe.get_doc('Customer Account', self.name, is_child = True)

			for contract_field in self.meta.fields:
				if contract_field.fieldname != 'name' and contract_field.fieldtype != 'Column Break' and contract_field.fieldtype != 'Section Break' and contract_field.fieldtype != 'Button':
					setattr(existing_account, contract_field.fieldname, getattr(self, contract_field.fieldname))

			existing_account.set('contracts', None)
			Document.save(existing_account)
