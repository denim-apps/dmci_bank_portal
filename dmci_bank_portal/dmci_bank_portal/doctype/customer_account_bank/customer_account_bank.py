# Copyright (c) 2021, JC Gurango and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe.email.doctype.email_template.email_template import get_email_template

class CustomerAccountBank(Document):
	def db_update(self, *args, **kwargs):
		is_new = self.get('__islocal')

		Document.db_update(self, *args, **kwargs)

		if is_new and self.parentfield == 'endorsed_banks':
			# Queue up email
			customer_account = frappe.get_doc('Customer Account', self.parent)
			dict = customer_account.get_valid_dict()
			dict['bank'] = self.bank
			email = get_email_template('Bank Endorsed', dict)

			# Find TLs of the bank
			emails = frappe.db.get_all('DMCI Portal User', filters={
				'bank': self.bank,
				'team_lead': True
			}, pluck='user')

			if len(emails) > 0:
				frappe.sendmail(
					recipients = emails,
					subject = email['subject'],
					message = email['message'],
				)
