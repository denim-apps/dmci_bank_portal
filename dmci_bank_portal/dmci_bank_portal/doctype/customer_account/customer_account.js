// Copyright (c) 2021, JC Gurango and contributors
// For license information, please see license.txt

const statusLabels = {
	email: 'Email Status',
	sms: 'SMS Status',
	callout: 'Call-Out Status',
};

const fieldPermissionDefs = {
	customer_name: {
		dmci_edit: false,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	customer_email_address: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	birthdate: {
		dmci_edit: false,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	customer_contact_numbers: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	citizenship: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	marital_status: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	co_buyer: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	spa: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	cancellation_reason: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	account_history: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	email_status: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	email_reason: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	date_emailed: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	sms_status: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	sms_reason: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	sms_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	callout_status: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	callout_reason: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	callout_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	new_bf_due_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	new_bf_due_year: {
		dmci_edit: false,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	new_bf_due_month: {
		dmci_edit: false,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	account_final_status: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	detailed_status: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	bank_application_detailed_status: {
		dmci_edit: false,
		dmci_view: false,
		bank_edit: true,
		bank_view: true,
	},
	bank_ctsf: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	credit_term: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	log_bfp: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	bfp_process_id: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	log_status: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: true,
		bank_view: true,
	},
	log_received_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	log_bank: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	log_amount: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	date_released: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	month_released: {
		dmci_edit: false,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	year_released: {
		dmci_edit: false,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	actual_cash: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	total_amount_due: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	outstanding_balance_for_drawdown: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	nbfd_process_id: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	bank_applied: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	bank_ao_name: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	estimated_incentive_amount: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	incentive_amount: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	incentive_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	incentive_year: {
		dmci_edit: false,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	incentive_month: {
		dmci_edit: false,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	incentive_scheme: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	incentive_status: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
		log_bank_edit: true,
		log_bank_view: true,
	},
	bir_2307_status: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	date_endorsed_by_developer: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	date_endorsed_by_branch: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	referred_by: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	referrer_name: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	log_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	log_notification_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	cts_requested_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	cts_received_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	date_soa_requested: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	date_soa_received: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	date_forwarded_to_remedial: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	date_received_from_remedial: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	signed_comp_sheet_forwarded_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	log_to_signatory_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	log_for_review_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	log_reviewed_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	out_signatory: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	date_forwarded_to_bank: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	interest_for_fast_5: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	fas_no: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	waiver_form: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	nbfd_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	treasurybank_advice: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	nbfd_to_credit: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	nbfd_to_dicd: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	back_to_ao_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	back_to_ao_return_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	back_to_ao_reason: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	back_to_ao_status: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	dordpa_email_sent_to_client: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	dordpa_received_forwarded: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	endorsement_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	disclosure_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	property: {
		dmci_edit: 'initial',
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	property_unit: {
		dmci_edit: 'initial',
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	search_name: {
		dmci_edit: 'initial',
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	building_name: {
		dmci_edit: 'initial',
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	contract_value: {
		dmci_edit: 'initial',
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	customer_account: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	contracts: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	bank_assignments: {
		dmci_edit: false,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	contract_number: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	reservation_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	contract_status: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	outstanding_balance: {
		dmci_edit: false,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	bank_financing_amount_due: {
		dmci_edit: false,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	closing_fee_amount: {
		dmci_edit: false,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	bank_financing_due_date: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	customer_address: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	dmci_team: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	dmci_ao: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	endorsed_banks: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	principal_buyer_name: {
		dmci_edit: false,
		dmci_view: true,
		bank_edit: false,
		bank_view: true,
	},
	approved_bank: {
		dmci_edit: true,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	status_of_account: {
		dmci_edit: false,
		dmci_view: true,
		bank_edit: false,
		bank_view: false,
	},
	bank_ao: {
		dmci_edit: false,
		dmci_view: false,
		bank_edit: true,
		bank_view: true,
	},
};

frappe.ui.form.on('Customer Account', {
	refresh: function (frm) {
		frappe.jcgurango.registerCascadingSelects(frm, 'callout_reason');
		frappe.jcgurango.registerCascadingSelects(frm, 'detailed_status');
		frappe.jcgurango.registerCascadingSelects(frm, 'bank_application_detailed_status');

		$('*[data-fieldname="contracts"]').find('.grid-remove-rows').hide();

		const commsHistorySectionBody = $('[data-fieldname=communication_history]').parent().parent().parent();
		let $commsHistoryView = $('#commsHistory');

		if (!$commsHistoryView.length) {
			$commsHistoryView = $('<div class="form-column col-sm-12 mb-4" id="commsHistory"></div>');
			commsHistorySectionBody.append($commsHistoryView);
		}

		const commsHistory = JSON.parse(frm.doc.communication_history || '[]');
		const commsHistoryHtml = `
		<div class="new-timeline comms">
			<div class="timeline-items">
				${commsHistory.map(({ type, status, reason, date }) => `
					<div class="timeline-item">
						<div class="timeline-dot"></div>
						<div class="timeline-content ">
								<b>${statusLabels[type]}</b>: <b>${status}</b> ${reason ? `(${reason})` : ''}
								<span> - <b>${(new Date(date)).toLocaleDateString()}</b></span>
							</div>
					</div>
				`).join('\n')}
				<div class="timeline-item">
					<div class="timeline-dot"></div>
					<div class="timeline-content ">
							<button class="btn btn-xs btn-secondary btn-hide-history">Hide History</button>
						</div>
				</div>
			</div>
		</div>
		`;

		const showComms = function () {
			$commsHistoryView.html(commsHistoryHtml).find('.btn-hide-history').click(hideComms);
		};

		const hideComms = function () {
			$commsHistoryView.html(`
				<div class="new-timeline comms">
					<div class="timeline-items">
						<div class="timeline-item">
							<div class="timeline-dot"></div>
							<div class="timeline-content ">
									<button class="btn btn-xs btn-secondary btn-show-history">Show History</button>
								</div>
						</div>
					</div>
				</div>
			`).find('.btn-show-history').click(showComms);
		};

		hideComms();

		let $endorseButton = $('#endorse');

		if (!$endorseButton.length) {
			$endorseButton = $('<button class="btn btn-xs btn-primary" style="margin-left: 4px;" id="endorse">Endorse</button>');
			frm.get_field('contracts').grid.grid_buttons.append($endorseButton);

			$endorseButton.click(function () {
				frappe.dmci.endorse(frm.get_selected().contracts, function () {
					frm.reload_doc();
				});
			});
		}

		let $awardButton = $('#award');

		if (!$awardButton.length) {
			$awardButton = $('<button class="btn btn-xs btn-primary" style="margin-left: 4px;" id="award">Award</button>');
			frm.get_field('contracts').grid.grid_buttons.append($awardButton);

			$awardButton.click(function () {
				frappe.dmci.award(frm.get_selected().contracts, function () {
					frm.reload_doc();
				});
			});
		}

		const hasSelected = function () {
			const selection = frm.get_selected();

			return selection && selection.contracts && selection.contracts.length > 0;
		};

		frm.get_field('contracts').grid.wrapper.on('click', '.grid-row-check', function () {
			$endorseButton.toggleClass('hidden', !hasSelected());
			$awardButton.toggleClass('hidden', !hasSelected());
		});

		$endorseButton.toggleClass('hidden', !hasSelected());
		$awardButton.toggleClass('hidden', !hasSelected());

		const contractsGrid = frm.get_field('contracts').grid;

		contractsGrid.wrapper.on('change', function () {
			contractsGrid.grid_rows.forEach(function (row) {
				if (!row.doc.__islocal) {
					const duplicateButton = $(`
					<div class="btn-open-row">
						<a>
							<svg class="icon  icon-xs" style="">
								<use class="" href="#icon-duplicate"></use>
							</svg>
						</a>
						<div class="hidden-xs edit-grid-row">${__('Duplicate')}</div>
					</div>
					`).click(function (e) {
						e.preventDefault();
						e.stopPropagation();
						contractsGrid.add_new_row(
							contractsGrid.grid_rows.length + 1,
							null,
							true,
							{
								...row.doc,
								reference_row: row.doc.name,
								contract_number: null,
							},
						);
					});

					row.open_form_button.after(duplicateButton).remove();
				}
			});
		}).change();
	},
	contracts_on_form_rendered: function (frm) {
		if (!frm.selected_doc.__islocal) {
			const form = frappe.ui.form.get_open_grid_form();

			if (form) {
				form.hide_form();
			}

			frappe.set_route('Form', 'Customer Account', frm.selected_doc.name);
		}
	},
	onload: function (frm) {
		!cur_frm.fields_dict["bank_financing_section"].is_collapsed() && frm.fields_dict["bank_financing_section"].collapse();
		!cur_frm.fields_dict["bank_incentive_section"].is_collapsed() && frm.fields_dict["bank_incentive_section"].collapse();
		!cur_frm.fields_dict["dmci_section"].is_collapsed() && frm.fields_dict["dmci_section"].collapse();
		const isDmci = frappe.user_roles.includes('Account Officer');
		const isBank = frappe.user_roles.includes('Bank Officer');
		const isLogBank = isBank && frm.doc.log_bank;

		if (isDmci) {
			// Hide the bank_ao field.
			frm.set_df_property('bank_ao', 'hidden', true);
			frm.set_df_property('bank_application_detailed_status', 'hidden', true);
		}

		if (!frm.doc.__islocal && !frappe.user_roles.includes('System Manager')) {
			Object.keys(fieldPermissionDefs).forEach((field) => {
				const fieldObject = frm.get_field(field);
				let canView = false;
				let canEdit = false;

				if (isDmci) {
					canView = canView || fieldPermissionDefs[field].dmci_view;
					canEdit = canEdit || (fieldPermissionDefs[field].dmci_edit === 'initial' && !frm.doc[field]) || (fieldPermissionDefs[field].dmci_edit === true);
				}

				if (isBank) {
					canView = canView || fieldPermissionDefs[field].bank_view;
					canEdit = canEdit || fieldPermissionDefs[field].bank_edit;
				}

				if (isLogBank) {
					canView = canView || fieldPermissionDefs[field].log_bank_view;
					canEdit = canEdit || fieldPermissionDefs[field].log_bank_edit;
				}

				fieldObject.get_status = function(...args) {
					const status = frappe.ui.form.Control.prototype.get_status.apply(fieldObject, args);

					if (status === 'None' && canView) {
						return 'Read';
					}

					return status;
				};

				frm.set_df_property(field, 'hidden', !canView);
				frm.set_df_property(field, 'read_only', !canEdit);
			});
		}
	},
	before_save: function (frm) {
		if (!frm.doc.estimated_incentive_amount && frm.doc.actual_cash) {
			return frm.set_value('estimated_incentive_amount', frm.doc.actual_cash * 0.01);
		}
	},
	referred_by: function (frm) {
		if (frm.doc.referred_by === 'Developer' && !frm.doc.referrer_name) {
			frm.set_value('referrer_name', frappe.session.user);
		}
	},
	log_amount: function (frm) {
		if (frm.doc.log_amount > 0) {
			frm.set_value('log_bfp', true);
		}
	},
});

frappe.ui.form.on('Contract', 'view_details', function (frm) {
	const form = frappe.ui.form.get_open_grid_form();

	if (form) {
		form.hide_form();
	}

	frappe.set_route('Form', 'Customer Account', frm.selected_doc.name);
});
