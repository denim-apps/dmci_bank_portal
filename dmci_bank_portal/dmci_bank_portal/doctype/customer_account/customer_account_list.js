const columns = [
  'customer_account',
  'customer_name',
  'contract_number',
  'principal_buyer_name',
  'citizenship',
  'marital_status',
  'birthdate',
  'customer_address',
  'customer_contact_numbers',
  'customer_email_address',
  'co_buyer',
  'dmci_ao',
];

frappe.listview_settings['Customer Account'] = {
  add_fields: columns,
  list_width: 3000,
  get_columns: function(get_df) {
    const listColumns = columns.map(get_df).map((df) => ({
      type: 'Field',
      df,
    }));

    listColumns[0] = {
      type: 'Subject',
      df: {
        label: 'Customer Account',
        fieldname: 'name',
      },
    };

    return listColumns;
  },
  set_actions_menu_items(listView) {
    if (frappe.user_roles.includes('Account Officer')) {
      listView.page.add_actions_menu_item(
        __('Endorse'),
        () => {
          const items = listView.get_checked_items(true);
          frappe.dmci.endorse(items, function () {
            frappe.msgprint(__('Endorsement successful'));
            listView.refresh();
          });
        },
        false
      );

      listView.page.add_actions_menu_item(
        __('Award'),
        () => {
          const items = listView.get_checked_items(true);
          frappe.dmci.award(items, function () {
            frappe.msgprint(__('Award successful'));
            listView.refresh();
          });
        },
        false
      );
    }
  },
  make_standard_filters(fields) {
    fields[0].label = 'Customer Account';
  },
};
