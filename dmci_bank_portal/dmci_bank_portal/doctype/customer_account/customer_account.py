# Copyright (c) 2021, JC Gurango and contributors
# For license information, please see license.txt

# import frappe
from datetime import date as d
import json
from dmci_bank_portal import get_dmci_portal_user, is_active_dmci_portal_user
from dmci_bank_portal.permissions import customer_account_has_permission
from frappe.email.doctype.email_template.email_template import get_email_template
import frappe
from frappe.model.document import Document
import dateutil.parser as parser
from frappe import _

account_fields = [
  'customer_account',
  'customer_name',
  'citizenship',
  'marital_status',
  'birthdate',
  'customer_contact_numbers',
  'customer_email_address',
  'co_buyer',
  'spa',
  'cancellation_reason',
  'account_history',
	'dmci_ao',
	'dmci_team',
	'communication_history',
	'email_status',
	'email_reason',
	'date_emailed',
	'sms_status',
	'sms_reason',
	'sms_date',
	'callout_status',
	'callout_reason',
	'callout_date',
]

comms_types = ['email', 'sms', 'callout']

comms_fields = {
	'email': ['email_status', 'email_reason', 'date_emailed'],
	'sms': ['sms_status', 'sms_reason', 'sms_date'],
	'callout': ['callout_status', 'callout_reason', 'callout_date']
}

def get_bank_data(customer_account, bank):
	existing = frappe.db.get_value('Account Bank Data', filters={
		'customer_account': customer_account,
		'bank': bank,
	}, fieldname='name')

	if existing:
		doc = frappe.get_doc('Account Bank Data', existing)
		return doc

	return None

def parse_date(date):
	dt = date

	if not isinstance(date, d):
		dt = parser.parse(date)

	return dt

def get_month(date):
	return parse_date(date).strftime('%B')

def get_year(date):
	return parse_date(date).year

def send_email_notification(acc, recipient, sender, email_template = 'Contract Assigned'):
	# Send the notification
	dict = acc.get_valid_dict()
	dict['recipient_name'] = dict['bank_ao_name'] = frappe.db.get_value('User', filters={ 'name': recipient }, fieldname='full_name')
	dict['sender_name'] = dict['dmci_ao_name'] = frappe.db.get_value('User', filters={ 'name': sender }, fieldname='full_name')
	email = get_email_template(email_template, dict)

	if not recipient:
		return

	r = recipient if isinstance(recipient, str) else recipient.email

	if r and is_active_dmci_portal_user(r) and customer_account_has_permission(acc, r):
		frappe.sendmail(
			recipients = [r],
			subject = email['subject'],
			message = email['message'],
		)

def set_bank_data(customer_account, bank, update_data):
	existing = frappe.db.get_value('Account Bank Data', filters={
		'customer_account': customer_account,
		'bank': bank,
	}, fieldname='name')
	should_notify = False

	if existing:
		doc = frappe.get_doc('Account Bank Data', existing)

		if update_data.get('ao'):
			should_notify = doc.ao != update_data.get('ao')
	else:
		doc = frappe.new_doc('Account Bank Data')
		doc.customer_account = customer_account
		doc.bank = bank
		should_notify = True

	for field in update_data.keys():
		doc.set(field, update_data.get(field))

	doc.save(ignore_permissions=True)

	if should_notify:
		# Send the notification
		acc = frappe.get_doc('Customer Account', customer_account)
		send_email_notification(acc, update_data.get('ao'), acc.dmci_ao)

class CustomerAccount(Document):
	def __init__(self, *args, **kwargs):
		Document.__init__(self, *args, **kwargs)
		self._is_child = kwargs.get('is_child')
		user = frappe.session.user
		
		if user:
			portal_user = get_dmci_portal_user(user)
		
			if portal_user.bank and not 'bank_ao' in kwargs:
				data = get_bank_data(self.name, portal_user.bank)

				if data:
					self.bank_ao = data.ao
					self.bank_application_detailed_status = data.bank_application_detailed_status


	def update_child_table(self, fieldname, df=None):
		"""sync child table for given fieldname (origin: apps/frappe/frappe/model/document.py)"""
		rows = []
		if not df:
			df = self.meta.get_field(fieldname)

		for d in self.get(df.fieldname):
			d.db_update()
			rows.append(d.name)

		if df.options in (self.flags.ignore_children_type or []):
			# do not delete rows for this because of flags
			# hack for docperm :(
			return

		if fieldname == 'endorsed_banks':
			if rows:
				deleted_rows = frappe.db.sql("""select bank from `tab{0}` where parent=%s
					and parenttype=%s and parentfield=%s
					and name not in ({1})""".format(df.options, ','.join(['%s'] * len(rows))),
						[self.name, self.doctype, fieldname] + rows)

				if len(deleted_rows) > 0:
					frappe.db.delete('Account Bank Data', {"customer_account": self.name, "bank": ("in", tuple(row[0] for row in deleted_rows))})
			else:
				frappe.db.delete('Account Bank Data', {
					"customer_account": self.name
				})

		if rows:
			# select rows that do not match the ones in the document
			deleted_rows = frappe.db.sql("""select name from `tab{0}` where parent=%s
				and parenttype=%s and parentfield=%s
				and name not in ({1})""".format(df.options, ','.join(['%s'] * len(rows))),
					[self.name, self.doctype, fieldname] + rows)
			if len(deleted_rows) > 0:
				# delete rows that do not match the ones in the document
				frappe.db.delete(df.options, {"name": ("in", tuple(row[0] for row in deleted_rows))})

		else:
			# no rows found, delete all rows
			frappe.db.delete(df.options, {
				"parent": self.name,
				"parenttype": self.doctype,
				"parentfield": fieldname
			})

	def load_from_db(self):
		"""Load document and children from database and create properties
		from fields (origin: apps/frappe/frappe/model/document.py)"""
		if not getattr(self, "_metaclass", False) and self.meta.issingle:
			single_doc = frappe.db.get_singles_dict(self.doctype)
			if not single_doc:
				single_doc = frappe.new_doc(self.doctype).as_dict()
				single_doc["name"] = self.doctype
				del single_doc["__islocal"]

			super(Document, self).__init__(single_doc)
			self.init_valid_columns()
			self._fix_numeric_types()

		else:
			d = frappe.db.get_value(self.doctype, self.name, "*", as_dict=1, for_update=self.flags.for_update)
			if not d:
				frappe.throw(_("{0} {1} not found").format(_(self.doctype), self.name), frappe.DoesNotExistError)

			super(Document, self).__init__(d)

		if self.name=="DocType" and self.doctype=="DocType":
			from frappe.model.meta import DOCTYPE_TABLE_FIELDS
			table_fields = DOCTYPE_TABLE_FIELDS
		else:
			table_fields = self.meta.get_table_fields()

		for df in table_fields:
			if df.fieldname == 'contracts':
				children = frappe.db.get_list(
						'Customer Account',
						filters={"customer_account": self.customer_account},
						fields=[
							'name',
							'contract_number',
							'status_of_account',
							'principal_buyer_name',
							'reservation_date',
							'contract_status',
							'outstanding_balance',
							'bank_financing_amount_due',
							'closing_fee_amount',
							'bank_financing_due_date',
							'customer_address'
						],
						order_by='contract_number asc'
					)
			elif df.fieldname == 'bank_assignments':
				children = frappe.db.get_all(
						'Account Bank Data',
						filters={"customer_account": self.name},
						fields=[
							'bank',
							'ao',
							'bank_application_detailed_status'
						],
						order_by='bank asc'
					)
			else:
				children = frappe.db.get_values(df.options,
					{"parent": self.name, "parenttype": self.doctype, "parentfield": df.fieldname},
					"*", as_dict=True, order_by="idx asc")

			if children:
				self.set(df.fieldname, children)
			else:
				self.set(df.fieldname, [])

		# sometimes __setup__ can depend on child values, hence calling again at the end
		if hasattr(self, "__setup__"):
			self.__setup__()

	def save(self, *args, **kwargs):
		communication_history = json.loads(self.communication_history or '[]')

		# Remove computed field values
		self.status_of_account = 'Pending'

		if self.bank_financing_due_date:
			if self.log_received_date:
				self.status_of_account = 'Awarded'
			else:
				self.status_of_account = 'Awaiting LOG'

		if self.new_bf_due_date:
			self.new_bf_due_month = get_month(self.new_bf_due_date)
			self.new_bf_due_year = get_year(self.new_bf_due_date)
		else:
			self.new_bf_due_month = None
			self.new_bf_due_year = None

		if self.date_released:
			self.month_released = get_month(self.date_released)
			self.year_released = get_year(self.date_released)
		else:
			self.month_released = None
			self.year_released = None

		if self.incentive_date:
			self.incentive_month = get_month(self.incentive_date)
			self.incentive_year = get_year(self.incentive_date)
		else:
			self.incentive_month = None
			self.incentive_year = None

		# Look for differences in comms fields
		if self.name and not self.get('__islocal'):
			old_record = frappe.get_doc('Customer Account', self.name)

			for comms_type in comms_types:
				fields = comms_fields[comms_type]
				changed = False

				for field in fields:
					if str(getattr(old_record, field)) != str(getattr(self, field)):
						changed = True

				if changed:
					communication_history.append({
						'type': comms_type,
						'status': getattr(self, fields[0]),
						'reason': getattr(self, fields[1]),
						'date': getattr(self, fields[2]),
					})

			self.communication_history = json.dumps(communication_history)

		# Cascade any account setup field to other records
		other_accounts = frappe.db.get_list('Customer Account',
			filters = {
				'customer_account': self.customer_account,
				'contract_number': ['!=', self.contract_number],
			},
			order_by = 'contract_number asc',
		)

		user = frappe.session.user
		reset_ao = self.bank_ao
		reset_status = self.bank_application_detailed_status

		if user:
			portal_user = get_dmci_portal_user(user)

			if portal_user.bank:
				set_bank_data(self.name, portal_user.bank, {
					'ao': self.bank_ao,
					'bank_application_detailed_status': self.bank_application_detailed_status
				})
				self.bank_ao = None
				self.bank_application_detailed_status = None

		current_dmci_ao = frappe.db.get_value('Customer Account', filters={ 'name': self.name }, fieldname='dmci_ao')

		if self.dmci_ao and self.dmci_ao != current_dmci_ao:
			# Send the notification
			send_email_notification(self, self.dmci_ao, frappe.session.user)

		Document.save(self, *args, **kwargs)

		for other_account in other_accounts:
			other_record = frappe.get_doc('Customer Account', other_account.name)

			for field in account_fields:
				setattr(other_record, field, getattr(self, field))

			Document.save(other_record, *args, **kwargs)

		self.load_from_db()
		self.bank_ao = reset_ao
		self.bank_application_detailed_status = reset_status

		if self.dmci_ao:
			send_email_notification(self, self.dmci_ao, self.bank_ao, 'Contract Updated')
