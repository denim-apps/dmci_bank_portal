import frappe

def execute():
  frappe.db.sql("UPDATE `tabCustomer Account` SET new_bf_due_month = CASE WHEN new_bf_due_month IS NOT NULL THEN MONTHNAME(CONCAT('2021-', new_bf_due_month, '-01')) ELSE NULL END;")
  frappe.db.commit()
