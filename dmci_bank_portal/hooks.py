from . import __version__ as app_version

app_name = "dmci_bank_portal"
app_title = "DMCI Bank Portal"
app_publisher = "JC Gurango"
app_description = "DMCI Bank Portal module."
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "jc@jcgurango.com"
app_license = "MIT"

app_include_css = "/assets/dmci_bank_portal/css/dmci_bank_portal.css"
app_include_js = "/assets/dmci_bank_portal/js/dmci_bank_portal.js"

permission_query_conditions = {
  "Customer Account": "dmci_bank_portal.permissions.customer_account_filter",
  "User": "dmci_bank_portal.permissions.user_filter",
}

has_permission = {
  "Customer Account": "dmci_bank_portal.permissions.customer_account_has_permission",
  "Contract": "dmci_bank_portal.permissions.contract_has_permission"
}

override_whitelisted_methods = {
  "frappe.lark.login_callback": "dmci_bank_portal.login_callback"
}

website_redirects = [
  {"source": "/login", "target": "/api/method/frappe.lark.login"},
]

website_route_rules = [
  {"from_route": "/internal/login", "to_route": "/login"},
]

doc_events = {
	"User": {
		"after_rename": "dmci_bank_portal.permissions.user_after_rename"
  }
}

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/dmci_bank_portal/css/dmci_bank_portal.css"
# app_include_js = "/assets/dmci_bank_portal/js/dmci_bank_portal.js"

# include js, css files in header of web template
# web_include_css = "/assets/dmci_bank_portal/css/dmci_bank_portal.css"
# web_include_js = "/assets/dmci_bank_portal/js/dmci_bank_portal.js"

# include custom scss in every website theme (without file extension ".scss")
# website_theme_scss = "dmci_bank_portal/public/scss/website"

# include js, css files in header of web form
# webform_include_js = {"doctype": "public/js/doctype.js"}
# webform_include_css = {"doctype": "public/css/doctype.css"}

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Jinja
# ----------

# add methods and filters to jinja environment
# jinja = {
# 	"methods": "dmci_bank_portal.utils.jinja_methods",
# 	"filters": "dmci_bank_portal.utils.jinja_filters"
# }

# Installation
# ------------

# before_install = "dmci_bank_portal.install.before_install"
# after_install = "dmci_bank_portal.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "dmci_bank_portal.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# DocType Class
# ---------------
# Override standard doctype classes

# override_doctype_class = {
# 	"ToDo": "custom_app.overrides.CustomToDo"
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"dmci_bank_portal.tasks.all"
# 	],
# 	"daily": [
# 		"dmci_bank_portal.tasks.daily"
# 	],
# 	"hourly": [
# 		"dmci_bank_portal.tasks.hourly"
# 	],
# 	"weekly": [
# 		"dmci_bank_portal.tasks.weekly"
# 	],
# 	"monthly": [
# 		"dmci_bank_portal.tasks.monthly"
# 	],
# }

# Testing
# -------

# before_tests = "dmci_bank_portal.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "dmci_bank_portal.event.get_events"
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "dmci_bank_portal.task.get_dashboard_data"
# }

# exempt linked doctypes from being automatically cancelled
#
# auto_cancel_exempted_doctypes = ["Auto Repeat"]


# User Data Protection
# --------------------

# user_data_fields = [
# 	{
# 		"doctype": "{doctype_1}",
# 		"filter_by": "{filter_by}",
# 		"redact_fields": ["{field_1}", "{field_2}"],
# 		"partial": 1,
# 	},
# 	{
# 		"doctype": "{doctype_2}",
# 		"filter_by": "{filter_by}",
# 		"partial": 1,
# 	},
# 	{
# 		"doctype": "{doctype_3}",
# 		"strict": False,
# 	},
# 	{
# 		"doctype": "{doctype_4}"
# 	}
# ]

# Authentication and authorization
# --------------------------------

# auth_hooks = [
# 	"dmci_bank_portal.auth.validate"
# ]

