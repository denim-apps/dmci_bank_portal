frappe.dmci = {
  endorse: function (contracts, callback) {
    const endorseDialog = new frappe.ui.Dialog({
      title: 'Endorse Contracts',
      fields: [
        {
          label: 'HTML',
          fieldname: 'html',
          fieldtype: 'HTML',
          options: `Endorse contract${contracts.length === 1 ? '' : 's'} ${contracts
            .map((contract, i) => i > 0 && i === (contracts.length - 1) ? ` and <b>${contract}</b>` : `<b>${contract}</b>`)
            .join(', ')
            } to...`,
        },
        {
          label: 'Banks',
          fieldname: 'banks',
          fieldtype: 'Table MultiSelect',
          options: 'Customer Account Bank',
          reqd: true,
        },
      ],
      primary_action_label: 'Endorse',
      primary_action(values) {
        frappe.call({
          method: 'dmci_bank_portal.endorse',
          args: {
            contracts,
            banks: values.banks.map(({ bank }) => bank),
          },
          callback: function (r) {
            if (!r.exc) {
              endorseDialog.hide();
              callback && callback();
            }
          }
        });
      }
    });

    endorseDialog.show();
  },
  award: function (contracts, callback) {
    frappe.call({
      method: 'dmci_bank_portal.get_endorsed_banks',
      args: {
        contracts,
      },
      callback: function (r) {
        if (!r.exc) {
          const awardDialog = new frappe.ui.Dialog({
            title: 'Award Contracts',
            fields: [
              {
                label: 'HTML',
                fieldname: 'html',
                fieldtype: 'HTML',
                options: `Award contract${contracts.length === 1 ? '' : 's'} ${contracts
                  .map((contract, i) => i > 0 && i === (contracts.length - 1) ? ` and <b>${contract}</b>` : `<b>${contract}</b>`)
                  .join(', ')
                  } to...`,
              },
              {
                label: 'Bank',
                fieldname: 'bank',
                fieldtype: 'Link',
                options: 'Bank',
                reqd: true,
              },
            ],
            primary_action_label: 'Award',
            primary_action(values) {
              frappe.call({
                method: 'dmci_bank_portal.award',
                args: {
                  contracts,
                  bank: values.bank,
                },
                callback: function (r) {
                  if (!r.exc) {
                    awardDialog.hide();
                    callback && callback();
                  }
                }
              });
            }
          });

          awardDialog.get_field('bank').get_query = function() {
            return {
              filters: [
                ['name', 'in', r.message],
              ],
            };
          };

          awardDialog.show();
        }
      }
    });
  },
};

frappe.ui.form.on('Data Import', {
  refresh: function (frm) {
    if (frm.doc.__islocal) {
      frm.set_value('reference_doctype', 'Customer Account');
      frm.set_value('import_type', 'Insert New Records');
      frm.save();
    }
  },
});

$(document).on('startup', function () {
  if (frappe.user_roles[0] === 'Account Officer' || frappe.user_roles[0] === 'Bank Officer' || frappe.user_roles[0] === 'Accounts Uploader') {
    $('a.navbar-brand').attr('href', '/app/customer-account');

    if (!frappe.router.current_route || frappe.router.current_route[0] === '' || frappe.router.current_route[0] === 'Workspaces') {
      frappe.set_route('List', 'Customer Account');
    }

    document.querySelector('a[onclick="return frappe.app.logout()"]')?.remove();
  }
});
