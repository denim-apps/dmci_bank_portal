from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in dmci_bank_portal/__init__.py
from dmci_bank_portal import __version__ as version

setup(
	name="dmci_bank_portal",
	version=version,
	description="DMCI Bank Portal module.",
	author="JC Gurango",
	author_email="jc@jcgurango.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
